<?php

namespace O2Development\Admin;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    /**
     * @inheritdoc
     */
    public function boot()
    {
        $this->handleConfig();
        $this->handleMigrations();

        /*
                 if ($this->app->runningInConsole()) {
                     $this->commands([
                         FooCommand::class,
                         BarCommand::class,
                     ]);
                 }
        */
    }

    /**
     * @inheritdoc
     */
    public function register()
    {
        //
    }

    /**
     * @inheritdoc
     */
    public function provides()
    {
        return [];
    }

    /**
     * Publish and merge the config file.
     *
     * @return void
     */
    private function handleConfig()
    {
        $configPath = __DIR__ . '/../config/config.php';

        $this->publishes([$configPath => config_path('entities.php')]);

        $this->mergeConfigFrom($configPath, 'entities');
    }

    /**
     * Publish migrations.
     *
     * @return void
     */
    private function handleMigrations()
    {
        $migrations = [
            'CreateEntitiesTable' => 'create_entities_table',
            'CreateEntityMetasTable' => 'create_entity_metas_table'
        ];

        foreach ($migrations as $class => $file) {
            if (!class_exists($class)) {
                $timestamp = date('Y_m_d_His', time());

                $this->publishes([
                    __DIR__ . '/../database/migrations/' . $file . '.php.stub' =>
                        database_path('migrations/' . $timestamp . '_' . $file . '.php')
                ], 'migrations');
            }
        }
    }
}